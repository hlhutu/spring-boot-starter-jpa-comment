# spring-boot-stater-jpa-comment

### 介绍

为jpa生成注释，基于swagger注解，简单易用。

**😁简单**

代码极少，只有2个类。

**⚡快速**

自动读取swagger注解，如果你本来就用了swagger，几乎不需要改代码。

**🐳兼容**

基于Hibernate方言识别，兼容所有Hibernate支持的数据库方言。

**🍃支持**

支持SpringBoot2.x + java1.8、SpringBoot3.x + java17。

### 快速开始

#### 导入

```xml
<dependency>
    <groupId>com.tomdog</groupId>
    <artifactId>spring-boot-starter-jpa-comment</artifactId>
    <version>1.1</version>
</dependency>
```

> 版本 1.0 -> Spring Boot 2.x  
> 版本 1.1 -> Spring Boot 3.x

#### 注解

```java
@Data
@Entity
@ApiModel(description = "测试用户")
public class DemoUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("用户名")
    private String userName;

    @ApiModelProperty("布尔值")
    private Boolean bool;

    @ApiModelProperty("创建日期")
    private LocalDate createDate;
}
```

#### 重启

![img.png](img.png)

数据库注释已生成

![img_1.png](img_1.png)

❗注意必须重新建表才可使注释生效。