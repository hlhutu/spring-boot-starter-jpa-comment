package com.tomdog.jpa.comment;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 插件入口
 */
@Configuration
@ComponentScan
public class AutoConfig {
}
